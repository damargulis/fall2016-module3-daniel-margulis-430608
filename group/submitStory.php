<?php
  include 'accessDatabase.php';
  session_start();
  $title = $_POST['title'];
  $link = $_POST['url'];
  $commentary = $_POST['commentary'];
  $user_id = $_SESSION['user_id'];

  //csrf check
  if($_POST['token'] !== $_SESSION['token']){
    die("CSRF Attack Detected.");
  }

  //valid url check
  if($link){
    if (filter_var($link, FILTER_VALIDATE_URL) === false) {
      header('Location: submissionForm.php?error=url');
      exit;
    }
  }

  //insert new story
  $stmt = $mysqli->prepare("insert into story (user_id, url, title, commentary) values (?,?,?,?)");
  if(!$stmt){
	   printf("Query Prep Failed: %s\n", $mysqli->error);
	    exit;
  }
  $stmt->bind_param('isss', $user_id, $link, $title, $commentary);
  $stmt->execute();
  $stmt->close();
  header('Location: welcomePage.php');
  exit;
?>
