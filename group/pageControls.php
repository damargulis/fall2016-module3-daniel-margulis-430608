<?php
  if(!isset($_SESSION)){
    session_start();
  }
  //page = 1 if not set
  $page = isset($_GET['page']) ? $_GET['page'] : 1;
?>
<div class='box'>
  <form method='GET'>
  <?php

    //add in other get values if already there
    if(isset($_GET['story'])){
      echo "<input type='hidden' name='story' value='".$_GET['story']."' />";
    }
    if(isset($_GET['user'])){
      echo "<input type='hidden' name='user' value='".$_GET['user']."' />";
    }

    if($page > 1){
        echo "<button type='submit' name='page' value='".htmlentities($page - 1)."' >Prev Page</button>";
    }
    if($more_pages_available){
        echo "<button type='submit' name='page' value='".htmlentities($page + 1)."' >Next Page</button>";
    }
   ?>
 </form>
</div>
