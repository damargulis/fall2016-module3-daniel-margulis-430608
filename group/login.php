<?php
  include 'accessDatabase.php';

  session_start();


  $stmt = $mysqli->prepare("SELECT COUNT(*), id, password_hash FROM user WHERE username=?");
  if(!$stmt){
    printf("Query Prep Failed: %s\n", $mysqli->error);
    exit;
  }

  $stmt->bind_param('s', $user);
  $user = $_POST['username'];
  $stmt->execute();

  $stmt->bind_result($cnt, $user_id, $pwd_hash);
  $stmt->fetch();

  $pwd_guess = $_POST['password'];
  if( $cnt == 1 && crypt($pwd_guess, $pwd_hash)==$pwd_hash){
    //info correct, log user in
    $_SESSION['user_id'] = $user_id;
    $_SESSION['token'] = substr(md5(rand()), 0, 10);
    header('Location: welcomePage.php');
    exit;
  } else {
    //info incorrect, redirect with error
    header('Location: loginPage.php?error=login');
    exit;
  }
?>
