<?php
  include 'accessDatabase.php';
  session_start();

  $username = $_POST['username'];
  $password = $_POST['password'];

  //check if username already exists
  $stmt = $mysqli->prepare("select * from user where username=(?) LIMIT 1");
  if(!$stmt){
    printf("Query Prep Failed: %s\n", $mysqli->error);
    exit;
  }

  $stmt->bind_param('s', $username);
  $stmt->execute();
  $result = $stmt->get_result();
  if($result->fetch_assoc()){
    //user already exists
    header('Location: loginPage.php?error=register');
    exit;
  }
  $stmt->close();

  //insert new user record
  $stmt2 = $mysqli->prepare("insert into user (username, password_hash) values (?,?)");
  if(!$stmt2){
    printf("Query Prep Failed: %s\n", $mysqli->error);
    exit;
  }

  $stmt2->bind_param('ss', $username, crypt($password));
  $stmt2->execute();

  $id = $stmt2->insert_id;
  $stmt2->close();

  $_SESSION['user_id'] = $id;
  $_SESSION['token'] = substr(md5(rand()), 0, 10);
  header('Location: welcomePage.php');

?>
