<!DOCTYPE html>
<html>
  <head>
    <title>Reddit3.0</title>
    <meta charset="UTF-8">
    <link rel="stylesheet"
      type="text/css"
      href="style.css"
      />
  </head>
  <body>
    <?php
      include 'header.php';
      //display error if applicable
      if(isset($_GET['error']) && $_GET['error'] === 'url'){
        echo "<div class='box alert warning'>Invalid Url</div>";
      }
    ?>
    <div class='box'>
      <form action='submitStory.php' method='POST' >
        <input type="hidden" name="token" value="<?php echo $_SESSION['token'];?>" />
        <label>Title: </label><input type='text' name='title' required/><br>
        <label>Link (optional): </label><input type='text' name='url'/><br>
        <label>Commentary (optional): </label><textarea name='commentary' rows='10' cols='50'></textarea><br>
        <input type='submit' />
      </form>
      <form action='welcomePage.php' method='GET'>
        <button type='submit'>Cancel</button>
      </form>
    </div>
  </body>
</html>
