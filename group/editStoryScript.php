<?php
  include 'accessDatabase.php';
  session_start();

  //detect csrf
  if($_POST['token'] !== $_SESSION['token']){
    die("CSRF Attack Detected.");
  }

  $action = $_POST['action'];
  switch($action){
    case "submit":
      //user submits, submit to database
      $stmt = $mysqli->prepare('update story set title=(?), url=(?), commentary=(?) where id=(?)');
      if(!$stmt){
          printf("Query Prep Failed: %s\n", $mysqli->error);
          exit;
      }
      if(isset($_POST['url']) && $_POST['url'] !== ""){
        if(filter_var($_POST['url'], FILTER_VALIDATE_URL) === false){
          //url invalid, show error
          header('Location: viewStory.php?error=url&story='.htmlentities($_POST['story']));
          exit;
        }
      }
      $stmt->bind_param('sssi', $_POST['title'], $_POST['url'], $_POST['commentary'], $_POST['story']);
      $stmt->execute();

      header('Location: viewStory.php?story='.htmlentities($_POST['story']));
      exit;
      break;
    case "delete":
      //user deletes, delete story

      //deletes all comments attatched to story
      $stmt2 = $mysqli->prepare('delete from comment where story_id=(?)');
      if(!$stmt2){
          printf("Query Prep Failed: %s\n", $mysqli->error);
          exit;
      }
      $stmt2->bind_param('i', $_POST['story']);
      $stmt2->execute();

      //deletes story
      $stmt = $mysqli->prepare('delete from story where id=(?)');
      if(!$stmt){
          printf("Query Prep Failed: %s\n", $mysqli->error);
          exit;
      }
      $stmt->bind_param('i', $_POST['story']);
      $stmt->execute();
      header('Location: welcomePage.php');
      break;
    case "cancel":
      //user canceled -> redirect
      header('Location: viewStory.php?story='.htmlentities($_POST['story']));
      exit;
      break;
    default:
      header('Location: viewStory.php?story='.htmlentities($_POST['story']));
      exit;
  }



?>
