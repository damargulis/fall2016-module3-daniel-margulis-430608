<!DOCTYPE html>
<html>
  <head>
    <title>Reddit3.0</title>
    <meta charset="UTF-8">
    <link rel="stylesheet"
      type="text/css"
      href="style.css"
      />
  </head>
  <body>
    <?php

      include 'header.php';
      include 'accessDatabase.php';

      $user_id = $_SESSION['user_id'];
      $comment_id = $_GET['comment_id'];
      $story_id = $_GET['story'];

      //retreive original text
      $stmt = $mysqli->prepare('select user_id,text from comment where id=(?) LIMIT 1');
      if(!$stmt){
          printf("Query Prep Failed: %s\n", $mysqli->error);
          exit;
      }
      $stmt->bind_param('i',$comment_id);
      $stmt->execute();
      $stmt->bind_result($user,$text);
      if(!$stmt->fetch()){
        //comment doesnt exist->redirect to homepage
        header('Location: welcomePage.php');
        exit;
      }
      else if($user != $user_id){
        //user does not have access, redirect to homepage
        header('Location: welcomePage.php');
        exit;
      }else{
        echo "<div class='box'>
                <form action='editCommentScript.php' method='POST'>
                  <input type='hidden' name='token' value='".$_SESSION['token']."' />
                  <input type='hidden' name='comment_id' value='".htmlentities($comment_id)."'/>
                  <input type='hidden' name='story_id' value='".htmlentities($story_id)."'/>
                  <textarea name='text' rows='10' cols='50'>".htmlentities($text)."</textarea><br>
                  <input type='submit' name='action' value='submit'/>
                  <input type='submit' name='action' value='delete'/>
                  <input type='submit' name='action' value='cancel'/>
                </form>
              </div>";
      }

    ?>
  </body>
</html>
