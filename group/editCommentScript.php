<?php
  include 'accessDatabase.php';
  session_start();

  //check for csrf
  if($_POST['token'] !== $_SESSION['token']){
    die("CSRF Attack Detected.");
  }

  $action = $_POST['action'];
  switch($action){
    case "submit":
      //user submited edit, submit edit on database
      $stmt = $mysqli->prepare('update comment set text=(?) where id=(?)');
      if(!$stmt){
          printf("Query Prep Failed: %s\n", $mysqli->error);
          exit;
      }
      $stmt->bind_param('si', $_POST['text'], $_POST['comment_id']);
      $stmt->execute();
      break;
    case "delete":
      //user wants to delete comment, delete it
      $stmt = $mysqli->prepare('delete from comment where id=(?)');
      if(!$stmt){
          printf("Query Prep Failed: %s\n", $mysqli->error);
          exit;
      }
      $stmt->bind_param('i', $_POST['comment_id']);
      $stmt->execute();
      break;
    case "cancel":
      //user wants to cancel, do nothing, return to viewstory
      break;
    default:
  }

  $story_id = $_POST['story_id'];
  header('Location: viewStory.php?story='.htmlentities($story_id));
  exit;


?>
