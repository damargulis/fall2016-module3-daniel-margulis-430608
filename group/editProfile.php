<!DOCTYPE html>
<html>
  <head>
    <title>Reddit3.0</title>
    <meta charset="UTF-8">
    <link rel="stylesheet"
      type="text/css"
      href="style.css"
      />
  </head>
  <body>
    <?php
      include 'header.php';
      include 'submissionBox.php';
      include 'accessDatabase.php';

      $profile_id = $_GET['user'];
      //retreive user info
      $stmt = $mysqli->prepare("select username,tagline,occupation,address,hobbies from user where id=(?)");
      if(!$stmt){
          printf("Query Prep Failed: %s\n", $mysqli->error);
          exit;
      }
      $stmt->bind_param('i', $profile_id);

      $stmt->execute();
      $stmt->bind_result($profile_username,$tagline,$occupation,$address,$hobbies);

      if(!$stmt->fetch()){
        //User doesnt exists, redirect
        header('Location: welcomePage.php'));
        exit;
      }
      if($profile_id != $_SESSION['user_id']){
        //User does not have permission to edit, redirect to view page
        header('Location: profile.php?user='.htmlentities($profile_id));
        exit;
      }

    ?>
    <div class='box'>
      <form action='editProfileScript.php' method='POST'>
        <input type='hidden' name='token' value='<?php echo ($_SESSION['token']); ?>' />
        <label>Tagline: </label><input type='text' name='tagline' value='<?php echo htmlentities($tagline); ?> '/><br>
        <label>Occupation: </label><input type='text' name='occupation' value='<?php echo htmlentities($occupation); ?> '/><br>
        <label>Address: </label><input type='text' name='address' value='<?php echo htmlentities($address); ?>' /><br>
        <label>Hobbies: </label><input type='text' name='hobbies' value='<?php echo htmlentities($hobbies); ?>' /><br>
        <button type='submit' name='action' value='submit'>Submit</button>
        <button type='submit' name='action' value='cancel'>Cancel</button>
      </form>
    </div>
  </body>
</html>
