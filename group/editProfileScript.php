<?php
  include 'accessDatabase.php';
  session_start();
  //for csrf attack
  if($_POST['token'] !== $_SESSION['token']){
    die("CSRF Attack Detected.");
  }

  $action = $_POST['action'];
  switch($action){
    case "submit";
      //user submits, edit info
      $stmt = $mysqli->prepare('update user set tagline=(?), occupation=(?), address=(?), hobbies=(?) where id=(?)');
      if(!$stmt){
          printf("Query Prep Failed: %s\n", $mysqli->error);
          exit;
      }
      $stmt->bind_param('ssssi', $_POST['tagline'], $_POST['occupation'], $_POST['address'], $_POST['hobbies'], $_SESSION['user_id']);
      $stmt->execute();
      break;
    case "cancel":
      //user cancels, do nothing
      break;
    default:
      break;
  }
  header('Location: profile.php?user='.htmlentities($_SESSION['user_id']));
?>
