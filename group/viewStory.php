<!DOCTYPE html>
<html>
  <head>
    <title>Reddit3.0</title>
    <meta charset="UTF-8">
    <link rel="stylesheet"
      type="text/css"
      href="style.css"
    />
  </head>
  <body>
    <?php
      include 'header.php';
      include 'submissionBox.php';
      include 'accessDatabase.php';

      if(isset($_GET['error'])){
        if($_GET['error'] === 'url'){
          echo "<div class='box alert warning'>Invalid Url</div>";
        }
      }

      //get story info
      $story_id = $_GET['story'];
      $stmt = $mysqli->prepare("select title,url,commentary,user_id,user.username from story join user on (story.user_id=user.id) where story.id=(?) LIMIT 1");
      if(!$stmt){
          printf("Query Prep Failed: %s\n", $mysqli->error);
          exit;
      }
      $stmt->bind_param('i', $story_id);
      $stmt->execute();
      $result = $stmt->bind_result($title, $url, $commentary, $author_id ,$username);
      function makeProfileUrl($user_id){
        return 'http://ec2-54-201-84-1.us-west-2.compute.amazonaws.com/~damargulis/module3/newsSite/profile.php?user='.htmlentities($user_id);
      }

      if($stmt->fetch()){
        echo "<div class='box'>";
          if(trim($url) == ''){
            echo "<div class='title'>".htmlentities($title)."</div>";
          }else{
            echo "<div class='title'><a href='".$url."'>".htmlentities($title)."</a></div>";
          }
          echo "<div class='subtitle' >";
            echo "<a href='".makeProfileUrl($author_id)."'>".htmlentities($username)."</a>";
          echo "</div>";
          if(isset($_SESSION['user_id']) && $author_id == $_SESSION['user_id']){
            echo "<form class='button' action='editStory.php' method='GET'>";
              echo "<button type='submit' name='story' value='".htmlentities($story_id)."' >Edit</button>";
            echo "</form>";
          }
          echo "<form class='button-left' action='welcomePage.php' method='GET'>";
            echo "<button type='submit'>Home</button>";
          echo "</form>";
        echo "</div>";
        echo "<div class='box'>";
          echo htmlentities($commentary);
        echo "</div>";
      }else{
        header('Location: welcomePage.php');
        exit;
      }
        include 'commentSubmissionBox.php';

        include 'accessDatabase.php';

        //get comments for page

        $PER_PAGE = 5;
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $offset = ($page - 1) * $PER_PAGE;

        $stmt = $mysqli->prepare('select comment.id,text,timestamp,user.username,user_id from comment join user on (user_id=user.id) where story_id=(?) order by timestamp limit ? offset ?');
        if(!$stmt){
            printf("Query Prep Failed: %s\n", $mysqli->error);
            exit;
        }
        $PER_PAGE++;
        $stmt->bind_param('iii', $story_id, $PER_PAGE, $offset);
        $stmt->execute();
        $result = $stmt->bind_result($comment_id,$text,$timestamp,$username,$author_id);

        $user_id = isset($_SESSION['user_id']) ? $_SESSION['user_id'] : -1;
        echo "<div class='box'>";


        for($i = 1; $i < $PER_PAGE; $i++){
          if($stmt->fetch()){
            echo "<div class='box comment'>";
              echo "<div class='title'>".htmlentities($text)."</div>";
              echo "<div class='subtitle'>".htmlentities($timestamp)." by <a href='".makeProfileUrl($author_id)."' >".htmlentities($username)."</a></div>";
              if($user_id == $author_id){
                echo "<form class='button' action='editComment.php' method='GET'>
                        <input type='hidden' name='story' value='".htmlentities($story_id)."'/>
                        <button type='submit' name='comment_id' value='".htmlentities($comment_id)."'>Edit</button>
                      </form>";
              }
            echo "</div>";
          }
        }
        echo "</div>";
        $more_pages_available = $stmt->fetch();
        include 'pageControls.php';
      ?>
  </body>
</html>
