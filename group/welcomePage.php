<!DOCTYPE html>
<html>
  <head>
    <title>Reddit3.0</title>
    <meta charset="UTF-8">
    <link rel="stylesheet"
      type="text/css"
      href="style.css"
      />
  </head>
  <body>
    <?php
      include 'header.php';
      include 'submissionBox.php';
    ?>
    <div class='box'>
      <?php

        //get storys for page
        $PER_PAGE = 5;
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $offset = ($page - 1) * $PER_PAGE;

        function makeProfileUrl($user_id){
          return 'http://ec2-54-201-84-1.us-west-2.compute.amazonaws.com/~damargulis/module3/newsSite/profile.php?user='.htmlentities($user_id);
        }

        include 'accessDatabase.php';
        $stmt = $mysqli->prepare("select story.id,title,url,timestamp,user.username,user.id from story join user on (story.user_id=user.id) order by timestamp desc limit ? offset ?");
        if(!$stmt){
            printf("Query Prep Failed: %s\n", $mysqli->error);
            exit;
        }
        $PER_PAGE++;
        $stmt->bind_param('ii', $PER_PAGE, $offset);
        $stmt->execute();
        $stmt->bind_result($id, $title, $url, $timestamp, $username, $user_id);
        for($i = 1; $i < $PER_PAGE; $i++){
          if($stmt->fetch()){
            echo "<div class='story'>";
              if(trim($url) == ''){
                echo "<div class='title'>".htmlentities($title)."</div>";
              }else{
                echo "<div class='title'><a href='".$url."'>".htmlentities($title)."</a></div>";
              }
              echo "<div class='subtitle'>";
                echo "<div class='author'>Submitted By: <a href='".makeProfileUrl($user_id)."'>".htmlentities($username)."</a></div>";
                echo "<div class='timestamp'>At: ".htmlentities($timestamp)."</div>";
                echo "<form action='viewStory.php' method='GET'>";
                  echo "<button type='submit'  name='story' value='".htmlentities($id)."' >View Comments</button>";
                echo "</form>";
              echo "</div>";
            echo "</div>";
          }
        }
        $more_pages_available = $stmt->fetch();

      ?>
    </div>
    <?php
    include 'pageControls.php';

     ?>
  </body>
</html>
