<!DOCTYPE html>
<html>
  <head>
    <title>Reddit3.0</title>
    <meta charset="UTF-8">
    <link rel="stylesheet"
      type="text/css"
      href="style.css"
      />
  </head>
  <body>
    <?php
      include 'header.php';
      include 'submissionBox.php';
      include 'accessDatabase.php';


      //retreve user info
      $profile_id = $_GET['user'];
      $stmt = $mysqli->prepare("select username,tagline,occupation,address,hobbies from user where id=(?) LIMIT 1");
      if(!$stmt){
          printf("Query Prep Failed: %s\n", $mysqli->error);
          exit;
      }
      $stmt->bind_param('i', $profile_id);

      $stmt->execute();
      $stmt->bind_result($profile_username,$tagline,$occupation,$address,$hobbies);

      if(!$stmt->fetch()){
        //user does not exist, redirect
        header("Location: welcomePage.php");
        exit;
      }
    ?>
    <div class='box'>
      <div class='profile-title'>
        <?php echo htmlentities($profile_username); ?>
      </div>
      <div class='profile-subtitle' >
        <?php echo htmlentities($tagline) ?>
      </div>
    </div>
    <div class='box'>
      <table>
        <tr>
          <td>Occupation:</td>
          <td><?php echo htmlentities($occupation) ?></td>
        </tr>
        <tr>
          <td>Address:</td>
          <td><?php echo htmlentities($address) ?></td>
        </tr>
        <tr>
          <td>Hobbies:</td>
          <td><?php echo htmlentities($hobbies) ?></td>
        </tr>
      </table>
      <?php
        if(isset($_SESSION['user_id']) && $profile_id == $_SESSION['user_id']){
          echo "<form action='editProfile.php' method='GET'>";
            echo "<button type='submit' name='user' value='".htmlentities($profile_id)."' >Edit</button>";
          echo "</form>";        }
      ?>
    </div>
    <div class='box'>
      <div class='box'>
      Posts:
    </div>

      <?php
        $stmt->fetch();

        //retreive user storys for page

        $PER_PAGE = 5;
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $offset = ($page - 1) * $PER_PAGE;
        $stmt2 = $mysqli->prepare("select id, url, title, timestamp from story where user_id=(?) order by timestamp desc limit ? offset ?");
        if(!$stmt2){
            printf("Query Prep Failed: %s\n", $mysqli->error);
            exit;
        }
        $PER_PAGE++;
        $stmt2->bind_param('iii', $profile_id, $PER_PAGE, $offset);
        $stmt2->execute();
        $stmt2->bind_result($story_id, $url, $title, $timestamp);

        for($i = 1; $i < $PER_PAGE; $i++){
          if($stmt2->fetch()){
            echo "<div class='story'>";
            if(trim($url) == ''){
              echo "<div class='title'>".htmlentities($title)."</div>";
            }else{
              echo "<div class='title'><a href=".$url.">".htmlentities($title)."</a></div>";
            }
              echo "<div class='subtitle'>";
                echo "<div class='timestamp'>At: ".htmlentities($timestamp)."</div>";
                echo "<form action='viewStory.php' method='GET'>";
                  echo "<button type='submit'  name='story' value='".htmlentities($story_id)."' >View Comments</button>";
                echo "</form>";
              echo "</div>";
            echo "</div>";
          }
        }
        $more_pages_available = $stmt2->fetch();
       ?>
    </div>
    <?php
      include 'pageControls.php';
    ?>
  </body>
</html>
