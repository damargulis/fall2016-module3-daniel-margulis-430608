<!DOCTYPE html>
<html>
	<head>
		<title>Reddit3.0</title>
		<meta charset="UTF-8">
		<link rel="stylesheet"
			type="text/css"
			href="style.css"
			/>
	</head>
	<body>
		<h1>Reddit3.0</h1>
		<h3>Its Reddit but better!</h3>
		<?php
			if(isset($_GET['error']) && $_GET['error'] === 'login'){
				//show login error
				echo "<div class='box alert warning'>Incorrect Username or Password</div>";
			}else if(isset($_GET['error']) && $_GET['error'] === 'register'){
				//show register error
				echo "<div class='box alert warning'>Username taken</div>";
			}
		?>
		<div class='box left'>
			<h6>Login</h6>
			<form action='login.php' method='POST'>
				<label>Username:</label><input type='text' name='username' required/><br>
				<label>Password:</label><input type='password' name='password' required/><br>
				<input type='submit' value='Go' />
				</form>
		</div>
		<div class='box right'>
			<h6>Register</h6>
			<form action='register.php' method='POST'>
				<label>Username:</label><input type='text' name='username' required/><br>
				<label>Password:</label><input type='password' name='password' required/><br>
				<input type='submit' value='Get Started' />
			</form>
		</div>
		<div class='box clear'>
			<h6>Login As Guest</h6>
			<form action='welcomePage.php' method='GET'>
				<input type='submit' value='Enter Without Loging In'/>
			</form>
		</div>
	</body>
</html>
