<div class='box' >
  <h1>Dunder Mifflin Infinity</h1>
    <?php
      if(!isset($_SESSION)){
        session_start();
      }
      include 'accessDatabase.php';

      if(!isset($_SESSION['user_id'])){
        //no user
        echo "<div class='welcome'>Welcome.  Login or Register to submit content.</div>";
        echo "<form action='welcomePage.php' method='GET' class='button-left'>";
        echo "<input type='submit' value='Home'/>";
        echo "</form>";
      }else{
        //retreive user info
        $stmt = $mysqli->prepare("select username from user where id=(?) LIMIT 1");
        if(!$stmt){
          printf("Query Prep Failed: %s\n", $mysqli->error);
           exit;
        }
        $stmt->bind_param('i', $_SESSION['user_id']);
        $stmt->execute();
        $stmt->bind_result($username);
        $stmt->fetch();
        echo "<div class='welcome'>Welcome ".htmlentities($username)."</div>";
        if(isset($_GET['user'])){
          //on profile page
          echo "<form action='welcomePage.php' method='GET' class='button-left'>";
          echo "<input type='submit' value='Home'/>";
        }else{
          echo "<form action='profile.php' method='GET' class='button-left'>";
          echo "<button type='submit' name='user' value='".$_SESSION['user_id']."' >View Profile</button>";
        }
        echo "</form>";

      }

      echo "<form action='logout.php' method='POST' class='button'>";
      if(!isset($_SESSION['user_id'])){
        echo "<input type='submit' value='Login'/>";
      }else{
        echo "<input type='submit' value='Logout'/>";
      }
      echo "</form>";

    ?>
</div>
