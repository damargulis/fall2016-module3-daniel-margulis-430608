<?php
  include 'accessDatabase.php';

  session_start();

  $story_id = $_POST['story'];
  $user = $_SESSION['user_id'];
  $comment = $_POST['text'];
  //check for CSRF
  if($_SESSION['token'] !== $_POST['token']){
    die("CSRF Attack Detected");
  }

  //submit comment
  $stmt = $mysqli->prepare("insert into comment (user_id, story_id, text) values (?,?,?)");
  if(!$stmt){
    printf("Query Prep Failed: %s\n", $mysqli->error);
    exit;
  }
  $stmt->bind_param('iis', $user, $story_id, $comment);
  $stmt->execute();

  //redirect back to story
  header('Location: viewStory.php?story='.htmlentities($story_id));
  exit;
?>
