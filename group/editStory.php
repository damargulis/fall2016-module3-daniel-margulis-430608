<?php
  include 'accessDatabase.php';
  session_start();

  $user_id = $_SESSION['user_id'];
  $story_id = $_GET['story'];

  //retreive story information
  $stmt = $mysqli->prepare('select user_id,url,title,commentary from story where id=(?)');
  if(!$stmt){
      printf("Query Prep Failed: %s\n", $mysqli->error);
      exit;
  }
  $stmt->bind_param('i', $story_id);
  $stmt->execute();
  $stmt->bind_result($author_id, $url, $title, $commentary);
  if(!$stmt->fetch()){
    //story doesnt exists, redirect
    header('Location: welcomePage.php');
    exit;
  }else if($user_id != $author_id){
    //user does not have permission to edit, redirect
    header('Location: viewStory.php?story='.htmlentities($story_id));
    exit;
  }else{
    echo "<div class='box'>
            <form action='editStoryScript.php' method='POST'>
              <input type='hidden' name='token' value='".$_SESSION['token']."' />
              <input type='hidden' name='story' value='".htmlentities($story_id)."'/>
              <label>Title: </label><input type='text' name='title' value='".htmlentities($title)."' required/><br>
              <label>Link (optional): </label><input type='text' name='url' value='".htmlentities($url)."'/><br>
              <label>Commentary (optional): </label><textarea name='commentary' rows='10' cols='50' >".htmlentities($commentary)."</textarea><br>
              <input type='submit' name='action' value='submit'/>
              <input type='submit' name='action' value='delete'/>
              <input type='submit' name='action' value='cancel'/>
            </form>
          </div>";
  }

?>
